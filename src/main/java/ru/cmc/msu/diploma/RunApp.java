package ru.cmc.msu.diploma;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import ru.cmc.msu.diploma.entity.Data2020;
import ru.cmc.msu.diploma.repository.Data2020Repository;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

@Service
public class RunApp implements CommandLineRunner {
    @Autowired
    private Data2020Repository repository;

    @Override
    public void run(String... args) throws Exception {
        OffsetDateTime startDate = OffsetDateTime.parse("2020-02-02T00:00:00+00:00");
        OffsetDateTime endDate = OffsetDateTime.parse("2020-02-03T00:00:00+00:00");
        //OffsetDateTime endTime = OffsetDateTime.parse("2020-02-29T00:00:00+00", formatter);
        //OffsetDateTime endTime = OffsetDateTime.parse("2020-02-29");
        List<Data2020> byIdDateBetween = repository.findByIdDateBetween(startDate, endDate);
        System.out.println("hello!");
    }
}
