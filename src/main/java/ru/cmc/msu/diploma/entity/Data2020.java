package ru.cmc.msu.diploma.entity;

import java.time.OffsetDateTime;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "data_2020")
public class Data2020 {
    @EmbeddedId
    private Data2020Id id;

    @Column(name = "vestibul_id")
    private Integer vestibulId;

    @Column(name = "validator_id")
    private Integer validatorId;

    @Column(name = "cristal_num")
    private Long cristalNum;

    @Column(name = "ticket_type")
    private Integer ticketType;

    @Column(name = "passage_type")
    private Integer passageType;

    @Column(name = "ticket_order_num")
    private Integer ticketOrderNum;

    @Column(name = "ticket_remains_count")
    private Integer ticketRemainsCount;


    public Data2020Id getId() {
        return id;
    }

    public void setId(Data2020Id id) {
        this.id = id;
    }

    public Integer getVestibulId() {
        return vestibulId;
    }

    public void setVestibulId(Integer vestibulId) {
        this.vestibulId = vestibulId;
    }

    public Integer getValidatorId() {
        return validatorId;
    }

    public void setValidatorId(Integer validatorId) {
        this.validatorId = validatorId;
    }

    public Long getCristalNum() {
        return cristalNum;
    }

    public void setCristalNum(Long cristalNum) {
        this.cristalNum = cristalNum;
    }

    public Integer getTicketType() {
        return ticketType;
    }

    public void setTicketType(Integer ticketType) {
        this.ticketType = ticketType;
    }

    public Integer getPassageType() {
        return passageType;
    }

    public void setPassageType(Integer passageType) {
        this.passageType = passageType;
    }

    public Integer getTicketOrderNum() {
        return ticketOrderNum;
    }

    public void setTicketOrderNum(Integer ticketOrderNum) {
        this.ticketOrderNum = ticketOrderNum;
    }

    public Integer getTicketRemainsCount() {
        return ticketRemainsCount;
    }

    public void setTicketRemainsCount(Integer ticketRemainsCount) {
        this.ticketRemainsCount = ticketRemainsCount;
    }

}