package ru.cmc.msu.diploma.entity;

import org.hibernate.Hibernate;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Data2020Id implements Serializable {
    private static final long serialVersionUID = 7663269055988877652L;
    @Column(name = "date")
    private OffsetDateTime date;

    @Column(name = "ticket_num")
    private Long ticketNum;

    public OffsetDateTime getDate() {
        return date;
    }

    public void setDate(OffsetDateTime date) {
        this.date = date;
    }

    public Long getTicketNum() {
        return ticketNum;
    }

    public void setTicketNum(Long ticketNum) {
        this.ticketNum = ticketNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Data2020Id entity = (Data2020Id) o;
        return Objects.equals(this.date, entity.date) &&
            Objects.equals(this.ticketNum, entity.ticketNum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, ticketNum);
    }

}