package ru.cmc.msu.diploma.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.cmc.msu.diploma.repository.Data2020Repository;

import java.time.OffsetDateTime;

@RestController
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class DataController {
    @Autowired
    private Data2020Repository repository;


    @GetMapping("getPassangerCountBetweenDateByStations")
    public ResponseEntity<String> getPassangerCountBetweenDateByStations(
        @RequestParam(name = "startTime") OffsetDateTime startTime,
        @RequestParam(name = "endTime") OffsetDateTime endTime
        ) {

        return ResponseEntity.ok(
            repository.getPassengerCountBetweenDateByStations(startTime, endTime).toString()
        );
    }

    @GetMapping("getPassangerCountBetweenDateByStationsAndTicketType")
    public ResponseEntity<String> getPassangerCountBetweenDateByStationsAndTicketType(
        @RequestParam(name = "startTime") OffsetDateTime startTime,
        @RequestParam(name = "endTime") OffsetDateTime endTime,
        @RequestParam(name = "ticketType") int ticketType

    ) {
        return ResponseEntity.ok(
            repository.getPassengerCountBetweenDateByStationsAndTicketType(
                startTime, endTime, ticketType
            ).toString()
        );
    }

    @GetMapping("getPassangerCountBetweenDateByStationsAndTicketTypes")
    public ResponseEntity<String> getPassangerCountBetweenDateByStationsAndTicketTypes(
        @RequestParam(name = "startTime") OffsetDateTime startTime,
        @RequestParam(name = "endTime") OffsetDateTime endTime
    ) {
        return ResponseEntity.ok(
            repository.getPassengerCountBetweenDateByStationsAndTicketTypes(startTime, endTime).toString()
        );
    }

    @GetMapping("getPassangerCountBetweenDateByStationsWithTimeStep")
    public ResponseEntity<String> getPassangerCountBetweenDateByStationsWithTimeStep(
        @RequestParam(name = "startTime") OffsetDateTime startTime,
        @RequestParam(name = "endTime") OffsetDateTime endTime,
        @RequestParam(name = "lineCode") int lineCode,
        @RequestParam(name = "timeInterval") int timeInterval
    ) {
        return ResponseEntity.ok(
            repository.getPassengerCountBetweenDateByStationsWithTimeStep(
                startTime, endTime, lineCode, timeInterval
            ).toString()
        );
    }
}
